package nsu.digital.doctor.appointments.repository;

import nsu.digital.doctor.appointments.entity.Role;
import nsu.digital.doctor.appointments.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByIdAndRole(long id, Role role);
}
