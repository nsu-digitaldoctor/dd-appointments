package nsu.digital.doctor.appointments.repository;

import nsu.digital.doctor.appointments.entity.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface DoctorRepository extends JpaRepository<Doctor, Long> {


    @Query(" select d from Doctor d where d.doctor.id = ?1")
    Optional<Doctor> findByUserid(Long id);
}
