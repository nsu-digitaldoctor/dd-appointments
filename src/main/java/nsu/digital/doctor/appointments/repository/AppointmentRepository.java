package nsu.digital.doctor.appointments.repository;

import nsu.digital.doctor.appointments.entity.Appointment;
import nsu.digital.doctor.appointments.entity.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

    List<Appointment> findAppointmentsByDoctor(Doctor doctor);

    Optional<Appointment> findAppointmentByDoctorAndDate(Doctor doctor, LocalDateTime date);
}
