package nsu.digital.doctor.appointments.mapper;

import nsu.digital.doctor.appointments.entity.Doctor;
import nsu.digital.doctor.appointments.model.DoctorDto;
import org.springframework.stereotype.Component;

@Component
public class DoctorMapper {

    public DoctorDto mapToDto(Doctor doctor) {
        return DoctorDto.builder()
                .id(doctor.getId())
                .firstName(doctor.getDoctor().getFirstname())
                .middleName(doctor.getDoctor().getMiddleName())
                .lastName(doctor.getDoctor().getLastname())
                .specialty(doctor.getSpecialty())
                .price(doctor.getPrice())
                .hospitalName(doctor.getHospital().getName())
                .hospitalAddress(doctor.getHospital().getAddress())
                .build();
    }
}
