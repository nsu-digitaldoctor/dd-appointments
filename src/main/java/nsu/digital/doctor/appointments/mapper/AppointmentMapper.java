package nsu.digital.doctor.appointments.mapper;

import lombok.RequiredArgsConstructor;
import nsu.digital.doctor.appointments.entity.Appointment;
import nsu.digital.doctor.appointments.model.AppointmentDto;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AppointmentMapper {
    private final UserMapper userMapper;
    private final DoctorMapper doctorMapper;

    public AppointmentDto mapToDto(Appointment appointment) {
        return AppointmentDto.builder()
                .id(appointment.getId())
                .user(userMapper.mapToDto(appointment.getUser()))
                .doctor(doctorMapper.mapToDto(appointment.getDoctor()))
                .application(appointment.getApplication())
                .status(appointment.getStatus())
                .conclusion(appointment.getConclusion())
                .date(appointment.getDate())
                .build();
    }
}
