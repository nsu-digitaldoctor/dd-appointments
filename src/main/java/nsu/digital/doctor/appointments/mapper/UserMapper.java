package nsu.digital.doctor.appointments.mapper;

import nsu.digital.doctor.appointments.entity.User;
import nsu.digital.doctor.appointments.model.UserDto;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public User mapDtoToEntity(UserDto userDto) {
        return User.builder()
                .id(userDto.getId())
                .firstname(userDto.getFirstName())
                .middleName(userDto.getMiddleName())
                .lastname(userDto.getLastName())
                .build();
    }

    public UserDto mapToDto(User user) {
        return UserDto.builder()
                .id(user.getId())
                .firstName(user.getFirstname())
                .middleName(user.getMiddleName())
                .lastName(user.getLastname())
                .build();
    }
}
