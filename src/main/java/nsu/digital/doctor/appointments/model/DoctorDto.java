package nsu.digital.doctor.appointments.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DoctorDto {
    private Long id;
    private String firstName;
    private String middleName;
    private String lastName;
    private Integer price;
    private String specialty;
    private String hospitalName;
    private String hospitalAddress;
}
