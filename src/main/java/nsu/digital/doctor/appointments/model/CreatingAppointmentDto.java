package nsu.digital.doctor.appointments.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreatingAppointmentDto {
    private long id;

    private long userId;

    private long doctorId;

    private String application;

    private LocalDateTime date;
}
