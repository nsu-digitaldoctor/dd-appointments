package nsu.digital.doctor.appointments.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nsu.digital.doctor.appointments.entity.Status;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppointmentDto {
    private long id;

    private UserDto user;

    private DoctorDto doctor;

    private String application;

    private Status status;

    private String conclusion;

    private LocalDateTime date;
}
