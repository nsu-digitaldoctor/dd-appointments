package nsu.digital.doctor.appointments.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nsu.digital.doctor.appointments.entity.Appointment;
import nsu.digital.doctor.appointments.entity.Doctor;
import nsu.digital.doctor.appointments.entity.Role;
import nsu.digital.doctor.appointments.entity.Status;
import nsu.digital.doctor.appointments.entity.User;
import nsu.digital.doctor.appointments.exception.AppointmentNotFoundException;
import nsu.digital.doctor.appointments.exception.BadAppointmentStatusException;
import nsu.digital.doctor.appointments.exception.BusyAppointmentTime;
import nsu.digital.doctor.appointments.exception.DoctorNotFoundException;
import nsu.digital.doctor.appointments.exception.UserNotFoundException;
import nsu.digital.doctor.appointments.mapper.AppointmentMapper;
import nsu.digital.doctor.appointments.mapper.UserMapper;
import nsu.digital.doctor.appointments.model.AppointmentDto;
import nsu.digital.doctor.appointments.model.ConclusionDto;
import nsu.digital.doctor.appointments.model.CreatingAppointmentDto;
import nsu.digital.doctor.appointments.model.DeleteAppointmentDto;
import nsu.digital.doctor.appointments.model.UpdateStatusDto;
import nsu.digital.doctor.appointments.repository.AppointmentRepository;
import nsu.digital.doctor.appointments.repository.DoctorRepository;
import nsu.digital.doctor.appointments.repository.HospitalRepository;
import nsu.digital.doctor.appointments.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static nsu.digital.doctor.appointments.entity.Status.DONE;
import static nsu.digital.doctor.appointments.entity.Status.OPEN;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class AppointmentService {
    private final static String APPOINTMENT_SUCCESS_CREATION_SUBJECT =
            "Successful creation of a doctor's appointment from digital doctor";

    private final AppointmentRepository appointmentRepository;
    private final AppointmentMapper appointmentMapper;
    private final UserMapper userMapper;
    private final HospitalRepository hospitalRepository;
    private final DoctorRepository doctorRepository;
    private final EmailSenderService emailSenderService;
    private final UserRepository userRepository;

    public List<AppointmentDto> getAppointmentByDoctor(Long doctorId) {
        Doctor doctor = doctorRepository.findByUserid(doctorId).orElse(new Doctor());
        return doctor.getDoctorAppointments().stream().map(appointmentMapper::mapToDto).toList();
    }

    public List<AppointmentDto> getAppointmentByUser(Long userId) {
        User user = userRepository.findByIdAndRole(userId, Role.USER).orElse(new User());
        return user.getUserAppointments().stream().map(appointmentMapper::mapToDto).toList();
    }

    public List<AppointmentDto> getAllAppointments() {
        return appointmentRepository.findAll().stream().map(appointmentMapper::mapToDto).toList();
    }

    public void insertConclusion(ConclusionDto conclusionDto, Long appointmentId) {
        Appointment appointment = appointmentRepository.findById(appointmentId)
                .orElseThrow(() ->
                        new AppointmentNotFoundException(
                                String.format("Appointment with id = %d not found", appointmentId)));
        appointment.setConclusion(conclusionDto.getConclusion());
        appointment.setStatus(DONE);
        appointmentRepository.save(appointment);
    }

    public void updateStatus(UpdateStatusDto updateStatusDto, Long appointmentId) {
        Appointment appointment = appointmentRepository.findById(appointmentId)
                .orElseThrow(() ->
                        new AppointmentNotFoundException(
                                String.format("Appointment with id = %d not found", appointmentId)));

        if (appointment.getStatus() == OPEN && updateStatusDto.getStatus() == Status.DONE) {
            throw new BadAppointmentStatusException("DONE status is not allowed here, try any other status");
        }
        if (appointment.getStatus() == Status.ACCEPTED && updateStatusDto.getStatus() == OPEN) {
            throw new BadAppointmentStatusException("OPEN status is not allowed here, try any other status");
        }

        appointment.setStatus(updateStatusDto.getStatus());
        appointmentRepository.save(appointment);
    }

    public void createAppointment(CreatingAppointmentDto appointmentDto) {
        User user = userRepository.findByIdAndRole(appointmentDto.getUserId(), Role.USER)
                .orElseThrow(() ->
                        new UserNotFoundException(
                                String.format("User with id = %d not found", appointmentDto.getUserId())));
        Doctor doctor = doctorRepository.findById(appointmentDto.getDoctorId())
                .orElseThrow(() ->
                        new DoctorNotFoundException(
                                String.format("Doctor with id = %d not found", appointmentDto.getDoctorId())));
        appointmentRepository.findAppointmentByDoctorAndDate(doctor, appointmentDto.getDate())
                .ifPresent(a -> {throw new BusyAppointmentTime("The appointment time is occupied by another patient");});

        Appointment appointment = Appointment.builder()
                .id(appointmentDto.getId())
                .user(user)
                .doctor(doctor)
                .status(OPEN)
                .application(appointmentDto.getApplication())
                .date(appointmentDto.getDate())
                .build();
        appointmentRepository.save(appointment);
        log.info("Appointment create");

        emailSenderService.sendEmail(user.getEmail(), APPOINTMENT_SUCCESS_CREATION_SUBJECT,
                "Your appointment successfully created!");
    }

    public void deleteAppointment(DeleteAppointmentDto deleteAppointmentDto) {
        Appointment appointment = appointmentRepository.findById(deleteAppointmentDto.getId())
                .orElseThrow(() ->
                        new AppointmentNotFoundException(
                                String.format("Appointment with id = %d not found", deleteAppointmentDto.getId())));
        log.info("Appointment delete");

        appointmentRepository.delete(appointment);
    }

    public void updateAppointment(CreatingAppointmentDto appointmentDto) {
        Appointment appointment = appointmentRepository.findById(appointmentDto.getId())
                .orElseThrow(() ->
                        new AppointmentNotFoundException(
                                String.format("Appointment with id = %d not found", appointmentDto.getId())));

        Doctor doctor = doctorRepository.findById(appointmentDto.getDoctorId())
                .orElseThrow(() ->
                        new DoctorNotFoundException(
                                String.format("Doctor with id = %d not found", appointmentDto.getDoctorId())));

        appointmentRepository.findAppointmentByDoctorAndDate(doctor, appointmentDto.getDate())
                .ifPresent(a -> { if(a.getId() != appointmentDto.getId()) throw new BusyAppointmentTime("The appointment time is occupied by another patient");});

        appointment.setApplication(appointmentDto.getApplication());
        appointment.setDate(appointmentDto.getDate());
        appointment.setStatus(OPEN);
        appointmentRepository.save(appointment);
    }
}
