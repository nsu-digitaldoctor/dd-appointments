package nsu.digital.doctor.appointments.controller;

import lombok.extern.slf4j.Slf4j;
import nsu.digital.doctor.appointments.exception.AppointmentNotFoundException;
import nsu.digital.doctor.appointments.exception.BadAppointmentStatusException;
import nsu.digital.doctor.appointments.exception.BusyAppointmentTime;
import nsu.digital.doctor.appointments.exception.HospitalNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class ErrorControllerAdvice {

    @ExceptionHandler(HospitalNotFoundException.class)
    public ResponseEntity<String> handleHospitalNotFoundException(HospitalNotFoundException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
    }

    @ExceptionHandler(AppointmentNotFoundException.class)
    public ResponseEntity<String> handleAppointmentNotFoundException(AppointmentNotFoundException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
    }

    @ExceptionHandler(BusyAppointmentTime.class)
    public ResponseEntity<String> handleBusyTimeException(BusyAppointmentTime exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
    }

    @ExceptionHandler(BadAppointmentStatusException.class)
    public ResponseEntity<String> handleBadStatusException(BadAppointmentStatusException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
    }
}
