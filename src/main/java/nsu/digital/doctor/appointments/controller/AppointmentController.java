package nsu.digital.doctor.appointments.controller;

import lombok.RequiredArgsConstructor;
import nsu.digital.doctor.appointments.model.AppointmentDto;
import nsu.digital.doctor.appointments.model.CreatingAppointmentDto;
import nsu.digital.doctor.appointments.model.ConclusionDto;
import nsu.digital.doctor.appointments.model.DeleteAppointmentDto;
import nsu.digital.doctor.appointments.model.UpdateStatusDto;
import nsu.digital.doctor.appointments.service.AppointmentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("appointments")
@RequiredArgsConstructor
public class AppointmentController {
    private final AppointmentService appointmentService;

    @GetMapping("/by-doctor/{id}")
    public ResponseEntity<List<AppointmentDto>> getAppointmentsByDoctor(@PathVariable Long id) {
        return ResponseEntity.ok().body(appointmentService.getAppointmentByDoctor(id));
    }

    @GetMapping("/by-user/{id}")
    public ResponseEntity<List<AppointmentDto>> getAppointmentsByUser(@PathVariable Long id) {
        return ResponseEntity.ok().body(appointmentService.getAppointmentByUser(id));
    }

    @PostMapping("/insertConclusion/{appointmentId}")
    public ResponseEntity<HttpStatus> insertConclusion(
            @RequestBody ConclusionDto conclusionDto,
            @PathVariable Long appointmentId) {
        appointmentService.insertConclusion(conclusionDto, appointmentId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/updateStatus/{appointmentId}")
    public ResponseEntity<HttpStatus> updateStatus(
            @RequestBody UpdateStatusDto updateStatusDto,
            @PathVariable Long appointmentId) {
        appointmentService.updateStatus(updateStatusDto, appointmentId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("")
    public ResponseEntity<List<AppointmentDto>> getAllAppointments() {
        return ResponseEntity.ok().body(appointmentService.getAllAppointments());
    }

    @PostMapping("/create")
    public ResponseEntity<HttpStatus> createAppointment(@RequestBody CreatingAppointmentDto appointmentDto) {
        appointmentService.createAppointment(appointmentDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<HttpStatus> deleteAppointment(@RequestBody DeleteAppointmentDto deleteAppointmentDto) {
        appointmentService.deleteAppointment(deleteAppointmentDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<HttpStatus> updateAppointment(@RequestBody CreatingAppointmentDto appointmentDto) {
        appointmentService.updateAppointment(appointmentDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
