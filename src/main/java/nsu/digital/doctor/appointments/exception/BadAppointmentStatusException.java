package nsu.digital.doctor.appointments.exception;

public class BadAppointmentStatusException extends RuntimeException {
    public BadAppointmentStatusException(String message) {
        super(message);
    }
}
