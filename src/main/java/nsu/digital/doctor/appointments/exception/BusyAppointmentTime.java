package nsu.digital.doctor.appointments.exception;

public class BusyAppointmentTime extends RuntimeException {
    public BusyAppointmentTime(String message) {
        super(message);
    }
}
