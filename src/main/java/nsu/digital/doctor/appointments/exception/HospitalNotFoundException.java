package nsu.digital.doctor.appointments.exception;

public class HospitalNotFoundException extends RuntimeException {

    public HospitalNotFoundException(String message) {
        super(message);
    }
}
