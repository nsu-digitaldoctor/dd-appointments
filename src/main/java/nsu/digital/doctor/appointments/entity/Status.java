package nsu.digital.doctor.appointments.entity;

public enum Status {
    OPEN,
    ACCEPTED,
    REJECTED,
    DONE,
    EXPIRED
}
