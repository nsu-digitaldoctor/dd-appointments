package nsu.digital.doctor.appointments.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user_info")
public class User {
    @Id
    @GeneratedValue
    private long id;

    private String firstname;
    private String lastname;
    private String middleName;
    private String email;
    private String password;
    private String inn;

    @Enumerated(EnumType.STRING)
    private Role role;

    @OneToOne(mappedBy = "organization")
    private Hospital hospital;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Appointment> userAppointments;

}