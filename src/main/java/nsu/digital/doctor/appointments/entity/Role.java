package nsu.digital.doctor.appointments.entity;

public enum Role {
    USER, ADMIN, ORG, DOCTOR
}

