package nsu.digital.doctor.appointments.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "hospital")
public class Hospital {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String address;
    private Double rating;
    private String description;
    private String fullName;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "head_doctor", referencedColumnName = "id")
    private User headDoctor;

    @OneToMany(mappedBy = "hospital", cascade = CascadeType.ALL)
    private List<Doctor> doctors;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "organization", referencedColumnName = "id")
    private User organization;

}
